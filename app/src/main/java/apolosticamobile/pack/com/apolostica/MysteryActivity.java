package apolosticamobile.pack.com.apolostica;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MysteryActivity extends AppCompatActivity {

    private MediaPlayer mMediaPlayer = null;

    private int mCurrentPosition = 0;
    private ArrayList<Integer> glorious_mysteries = new ArrayList<>();
    private ArrayList<Integer> glorious_mysteries_pictures = new ArrayList<>();

    private ArrayList<Integer> sorrowful_mysteries = new ArrayList<>();
    private ArrayList<Integer> sorrowful_mysteries_pictures = new ArrayList<>();

    private ArrayList<Integer> luminous_mysteries = new ArrayList<>();
    private ArrayList<Integer> luminous_mysteries_pictures = new ArrayList<>();


    private ArrayList<Integer> joyful_mysteries = new ArrayList<>();
    private ArrayList<Integer> joyful_mysteries_pictures = new ArrayList<>();


    public static final int JOYFUL_MYSTERIES = 1;
    public static final int LUMINOUS_MYSTERIES = 2;
    public static final int SORROWFUL_MYSTERIES = 3;
    public static final int GLORIOUS_MYSTERIES = 4;

    private LinearLayout backgroundImage;


    private int SCREEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mystery);

        backgroundImage = (LinearLayout) findViewById(R.id.imgBack);

        glorious_mysteries.add(R.raw.brownintro);
        glorious_mysteries.add(R.raw.brownone);
        glorious_mysteries.add(R.raw.ourfather);
        glorious_mysteries.add(R.raw.browntwo);
        glorious_mysteries.add(R.raw.ourfather);
        glorious_mysteries.add(R.raw.brownthree);
        glorious_mysteries.add(R.raw.ourfather);
        glorious_mysteries.add(R.raw.brownfour);
        glorious_mysteries.add(R.raw.ourfather);
        glorious_mysteries.add(R.raw.brownfive);
        glorious_mysteries.add(R.raw.ourfather);

        glorious_mysteries_pictures.add(R.drawable.brownintro);
        glorious_mysteries_pictures.add(R.drawable.majd1);
        glorious_mysteries_pictures.add(R.drawable.majd1);
        glorious_mysteries_pictures.add(R.drawable.majd2);
        glorious_mysteries_pictures.add(R.drawable.majd2);
        glorious_mysteries_pictures.add(R.drawable.majd3);
        glorious_mysteries_pictures.add(R.drawable.majd3);
        glorious_mysteries_pictures.add(R.drawable.majd4);
        glorious_mysteries_pictures.add(R.drawable.majd4);
        glorious_mysteries_pictures.add(R.drawable.majd5);
        glorious_mysteries_pictures.add(R.drawable.majd5);

        sorrowful_mysteries.add(R.raw.purpleintro);
        sorrowful_mysteries.add(R.raw.purpleone);
        sorrowful_mysteries.add(R.raw.ourfather);
        sorrowful_mysteries.add(R.raw.purpletwo);
        sorrowful_mysteries.add(R.raw.ourfather);
        sorrowful_mysteries.add(R.raw.purplethree);
        sorrowful_mysteries.add(R.raw.ourfather);
        sorrowful_mysteries.add(R.raw.purplefour);
        sorrowful_mysteries.add(R.raw.ourfather);
        sorrowful_mysteries.add(R.raw.purplefive);
        sorrowful_mysteries.add(R.raw.ourfather);

        sorrowful_mysteries_pictures.add(R.drawable.purplintro);
        sorrowful_mysteries_pictures.add(R.drawable.ezen1);
        sorrowful_mysteries_pictures.add(R.drawable.ezen1);
        sorrowful_mysteries_pictures.add(R.drawable.ezen2);
        sorrowful_mysteries_pictures.add(R.drawable.ezen2);
        sorrowful_mysteries_pictures.add(R.drawable.ezen3);
        sorrowful_mysteries_pictures.add(R.drawable.ezen3);
        sorrowful_mysteries_pictures.add(R.drawable.ezen4);
        sorrowful_mysteries_pictures.add(R.drawable.ezen4);
        sorrowful_mysteries_pictures.add(R.drawable.ezen5);
        sorrowful_mysteries_pictures.add(R.drawable.ezen5);


        luminous_mysteries.add(R.raw.yellowintro);
        luminous_mysteries.add(R.raw.yellowone);
        luminous_mysteries.add(R.raw.ourfather);
        luminous_mysteries.add(R.raw.yellowtwo);
        luminous_mysteries.add(R.raw.ourfather);
        luminous_mysteries.add(R.raw.yellowthree);
        luminous_mysteries.add(R.raw.ourfather);
        luminous_mysteries.add(R.raw.yellowfour);
        luminous_mysteries.add(R.raw.ourfather);
        luminous_mysteries.add(R.raw.yellowfive);
        luminous_mysteries.add(R.raw.ourfather);

        luminous_mysteries_pictures.add(R.drawable.yellowintro);
        luminous_mysteries_pictures.add(R.drawable.nour1);
        luminous_mysteries_pictures.add(R.drawable.nour1);
        luminous_mysteries_pictures.add(R.drawable.nour2);
        luminous_mysteries_pictures.add(R.drawable.nour2);
        luminous_mysteries_pictures.add(R.drawable.nour3);
        luminous_mysteries_pictures.add(R.drawable.nour3);
        luminous_mysteries_pictures.add(R.drawable.nour4);
        luminous_mysteries_pictures.add(R.drawable.nour4);
        luminous_mysteries_pictures.add(R.drawable.nour5);
        luminous_mysteries_pictures.add(R.drawable.nour5);


        joyful_mysteries.add(R.raw.pinkintro);
        joyful_mysteries.add(R.raw.pinkone);
        joyful_mysteries.add(R.raw.ourfather);
        joyful_mysteries.add(R.raw.pinktwo);
        joyful_mysteries.add(R.raw.ourfather);
        joyful_mysteries.add(R.raw.pinkthree);
        joyful_mysteries.add(R.raw.ourfather);
        joyful_mysteries.add(R.raw.pinkfour);
        joyful_mysteries.add(R.raw.ourfather);
        joyful_mysteries.add(R.raw.pinkfive);
        joyful_mysteries.add(R.raw.ourfather);

        joyful_mysteries_pictures.add(R.drawable.pinkintro);
        joyful_mysteries_pictures.add(R.drawable.farah1);
        joyful_mysteries_pictures.add(R.drawable.farah1);
        joyful_mysteries_pictures.add(R.drawable.farah2);
        joyful_mysteries_pictures.add(R.drawable.farah2);
        joyful_mysteries_pictures.add(R.drawable.farah3);
        joyful_mysteries_pictures.add(R.drawable.farah3);
        joyful_mysteries_pictures.add(R.drawable.farah4);
        joyful_mysteries_pictures.add(R.drawable.farah4);
        joyful_mysteries_pictures.add(R.drawable.farah5);
        joyful_mysteries_pictures.add(R.drawable.farah5);

        Button btnBackToTwo = (Button) findViewById(R.id.btnBackToTwo);
        Button btnForwardToFourth = (Button) findViewById(R.id.btnForwardToFourth);
        Button btnpause = (Button) findViewById(R.id.btnPause);


        ImageView btn_joyful_mysteries = (ImageView)findViewById(R.id.joyful_mysteries);
        ImageView btn_luminous_mysteries = (ImageView)findViewById(R.id.luminous_mysteries);
        ImageView btn_sorrowful_mysteries = (ImageView)findViewById(R.id.sorrowful_mysteries);
        ImageView btn_glorious_mysteries = (ImageView)findViewById(R.id.glorious_mysteries);


        btn_joyful_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopMp3();
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.JOYFUL_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        btn_luminous_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopMp3();
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.LUMINOUS_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        btn_sorrowful_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopMp3();
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.SORROWFUL_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        btn_glorious_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopMp3();
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.GLORIOUS_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        SCREEN = getIntent().getIntExtra("SCREEN", JOYFUL_MYSTERIES);
        playMp3();

        btnBackToTwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                stopMp3();
                destroyMediaPlayer();
                onBackPressed();
            }
        });
        btnpause.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pauseMp3();
            }
        });


        btnForwardToFourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentPosition++;
                playMp3();

            }
        });

    }

    public void playMp3() {

        switch (SCREEN) {

            case JOYFUL_MYSTERIES:
                playJoyfulMysteries();
                break;

            case LUMINOUS_MYSTERIES:
                playLuminousMysteries();
                break;

            case SORROWFUL_MYSTERIES:
                playSorrowfulMysteries();
                break;

            case GLORIOUS_MYSTERIES:

                playGloriousMysteries();
                break;
        }
    }

    private void onMediaPlayerCompletion() {
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                mCurrentPosition++;

                switch (SCREEN) {

                    case JOYFUL_MYSTERIES:
                        playJoyfulMysteries();
                        break;

                    case LUMINOUS_MYSTERIES:
                        playLuminousMysteries();
                        break;

                    case SORROWFUL_MYSTERIES:
                        playSorrowfulMysteries();
                        break;

                    case GLORIOUS_MYSTERIES:

                        playGloriousMysteries();
                        break;
                }
            }
        });
    }

    void stopMp3() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
        }
    }


    void pauseMp3() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
        } else {
            mMediaPlayer.start();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        destroyMediaPlayer();
    }

    public void destroyMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer = null;
        }
    }

    public void playJoyfulMysteries() {
        stopMp3();
        if (mCurrentPosition == joyful_mysteries.size()) {
            destroyMediaPlayer();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else {

            backgroundImage.setBackgroundResource(joyful_mysteries_pictures.get(mCurrentPosition));
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), joyful_mysteries.get(mCurrentPosition));
            mMediaPlayer.start();
            onMediaPlayerCompletion();
        }

    }

    public void playLuminousMysteries() {
        stopMp3();

        if (mCurrentPosition == luminous_mysteries.size()) {
            destroyMediaPlayer();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else {

            backgroundImage.setBackgroundResource(luminous_mysteries_pictures.get(mCurrentPosition));
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), luminous_mysteries.get(mCurrentPosition));
            mMediaPlayer.start();
            onMediaPlayerCompletion();
        }

    }

    public void playSorrowfulMysteries() {
        stopMp3();
        if (mCurrentPosition == sorrowful_mysteries.size()) {
            destroyMediaPlayer();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else {

            backgroundImage.setBackgroundResource(sorrowful_mysteries_pictures.get(mCurrentPosition));
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), sorrowful_mysteries.get(mCurrentPosition));
            mMediaPlayer.start();
            onMediaPlayerCompletion();
        }

    }

    public void playGloriousMysteries() {
        stopMp3();
        if (mCurrentPosition == glorious_mysteries.size()) {
            destroyMediaPlayer();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else {
            backgroundImage.setBackgroundResource(glorious_mysteries_pictures.get(mCurrentPosition));
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), glorious_mysteries.get(mCurrentPosition));
            mMediaPlayer.start();
            onMediaPlayerCompletion();
        }
    }
}
