package apolosticamobile.pack.com.apolostica;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

public class SplashActivity extends AppCompatActivity {
    private MediaPlayer mMediaPlayer = null;


    public static final int SPLASH_DURATION = 1000;
    private Handler mHandler;
    private Runnable mRunnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        LinearLayout imgFirstScreen = (LinearLayout) findViewById(R.id.mainScreen);
        mRunnable = new Runnable() {
            @Override
            public void run() {
                startActivity();
            }
        };

        imgFirstScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity();
            }
        });

        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, SPLASH_DURATION);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer = null;
        }
    }

    private void startActivity() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

}
