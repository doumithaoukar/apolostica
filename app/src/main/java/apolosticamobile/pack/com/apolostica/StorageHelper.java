package apolosticamobile.pack.com.apolostica;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sudha on 3/20/2016.
 */
public class StorageHelper {
    public static String GetLastClickedButtonIndex(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("mySettings", Context.MODE_PRIVATE);
        String mySetting = sharedPref.getString("LastClickedButton", null);
        return  mySetting;
    }

    public static void SetLastClickedButton(Context context, String butonId)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("mySettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("LastClickedButton", butonId);
        editor.commit();

    }
}
