package apolosticamobile.pack.com.apolostica;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.CrashManagerListener;
import net.hockeyapp.android.UpdateManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView glorious_mysteries = (ImageView) findViewById(R.id.glorious_mysteries);
        glorious_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.GLORIOUS_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        ImageView sorrowful_mysteries = (ImageView) findViewById(R.id.sorrowful_mysteries);
        sorrowful_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.SORROWFUL_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        ImageView luminous_mysteries = (ImageView) findViewById(R.id.luminous_mysteries);
        luminous_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.LUMINOUS_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        ImageView joyful_mysteries = (ImageView) findViewById(R.id.joyful_mysteries);
        joyful_mysteries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MysteryActivity.class);
                i.putExtra("SCREEN", MysteryActivity.JOYFUL_MYSTERIES);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        checkForUpdates();

    }

    private void checkForCrashes() {

        CrashManager.register(this, "6f813288be0145faaaf2a45f8740958b", new CrashManagerListener() {
            public boolean shouldAutoUploadCrashes() {
                return true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }


}
